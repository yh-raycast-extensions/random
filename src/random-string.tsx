import { Clipboard, Detail, showHUD, getPreferenceValues, PopToRootType } from "@raycast/api";
import { useEffect } from "react";
import { sample } from "./utils";

const CHARACTERS: ReadonlyArray<string> = "abcdefghijklmnopqrstuvwxyz".split("");

interface RawPreferences {
  defaultLength: string;
}

interface Preferences {
  defaultLength: number;
}

function getPreferences(): Preferences {
  const rawPreferences = getPreferenceValues<RawPreferences>();

  return {
    defaultLength: Number.parseInt(rawPreferences.defaultLength),
  };
}

function randomString(length: number): string {
  const characters: Array<string> = [];
  for (let i = 0; i < length; i += 1) {
    characters.push(sample(CHARACTERS));
  }
  return characters.join("");
}

export default function Command() {
  const { defaultLength } = getPreferences();
  const string = randomString(defaultLength);

  useEffect(() => {
    (async () => {
      await Clipboard.copy(string);
      await showHUD(`Copied ${string} to clipboard`, { clearRootSearch: true, popToRootType: PopToRootType.Immediate });
    })();
  }, []);

  return <Detail navigationTitle="Random String" isLoading={true} markdown="Generating a random string..." />;
}
