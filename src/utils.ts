export function sample<T>(elements: ReadonlyArray<T>): T {
  const idx = Math.floor(Math.random() * elements.length);
  return elements[idx];
}
